
/* eslint-disable import/no-extraneous-dependencies */

import contract from 'truffle-contract';
import umacContract from './Contract.json'

const getContract = async (web3) => {
    console.log(umacContract);
  const umac = contract(umacContract);
  umac.setProvider(web3.currentProvider);
  
  return umac.deployed();
};

export default getContract;