import React, { Component } from 'react';
import { Provider} from 'mobx-react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';

import RootStore from './stores/RootStore';

import Home from './view/home';
import Market from './view/market';
import Statistics from './view/statistics';
import PageNotFound from './notFound';
import Fund from './view/fund/index';
import Navbar from './view/navbar';
import getWeb3 from './utils/getWeb3';
import getContract from './utils/getContract';

const rootStore = new RootStore();

getWeb3
  .then(async (res) => {
    rootStore.web3Store.addWeb3(res.web3Instance);
    const contract = await getContract(res.web3Instance);
    rootStore.web3Store.addContract(contract)
    console.log(rootStore.web3Store.contract)
  })
  .catch(err => console.log(err));
  
class App extends Component {
  componentDidMount() {
    rootStore.openNotif('success', 'welcome', 'welcome to our pages')
  }
  render() {
    return (
      <Provider {...rootStore}>

      <div>
        <Navbar />
       <Router onUpdate={() => window.scrollTo(0, 0)}>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/market" component={Market} />
          <Route exact path="/market-stats" component={Statistics} />
          <Route exact path ="/invest" component={Fund} />
          <Route path='*' component={PageNotFound} />
          </Switch>
        </Router>
        </div>
      </Provider>
    );
  }
}

export default App;
