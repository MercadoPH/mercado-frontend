import React from 'react';
import { Input, Row, Col, Card, Avatar, Button } from 'antd';
import '../../App.css';

const Search = Input.Search;

const items = [{
  key: 1,
  name: "Kalamansi",
  price: 120,
  quantity: 5200,
  image: <img src ='./../img/kalamansi.jpg' width='250px' height='200px'/>,
  currentBid: 500
},
{
  key: 2,
  name: 'Sweet Corn',
  price: 750,
  quantity: 2000,
  image: <img src ='./../img/corn.jpg' width='250px' height='200px'/>,
  currentBid: 1500
},
{
  key: 3,
  name: 'Kamatis',
  price: 150,
  quantity: 932,
  image: <img src ='./../img/kamatis.jpg' width='250px' height='200px'/>,
  currentBid: 500
}, 
{ 
  key: 4,
  name: "Sili",
  price: 1250,
  quantity: 500,
  image: <img src ='./../img/sili.jpg' width='250px' height='200px'/>,
  currentBid: 2000
},
{
  key: 5,
  name: 'Patatas',
  price: 650,
  quantity: 2270,
  image: <img src ='./../img/patatas.jpg' width='250px' height='200px'/>,
  currentBid: 2500
},
{
  key: 6,
  name: 'Kamote #2',
  price: 550,
  quantity: 2932,
  image: <img src ='./../img/kamote.jpg' width='250px' height='200px'/>,
  currentBid: 1000
},
];


const Market = () => (
<div className="fieldBG2" style={{height: '40vh'}}>
<br/><br/><br/>
    <Row type="flex" align="center" justify="center">
      <Col>
        <h1 style={{textAlign:'center', color: '#fff', fontWeight:'200', fontSize: '2.5em'}}>Shop Now</h1>
        <Search className="search" placeholder="I am looking for ..."/>
      </Col>  
    </Row>
    <div>
    <Row type="flex" gutter={36} align="center" >
    {items.map(item => (
      <Col>
      <Card className="Card">
       {item.image} 
        <h1 className="HeroText3"><b>{item.name}</b></h1>
        <span>Initial Price: <b>P {item.price.toFixed(2)}</b></span><br/>
        <span>Current Bid: <b>{item.currentBid}</b></span><br/>
        <span>Quantity: <b>{item.quantity}</b></span><br/><br/>
        <Input placeholder="Input Bid" /> 
        <div style={{padding:'6px'}}>
        <Button type="primary" block>Place Bid</Button>
        </div>
      </Card>
      </Col>
    ))}
    </Row>
    <br/>
  </div>
  </div>
);

export default Market;