import React from 'react';
import { Menu, Icon, Button } from 'antd';

const Navbar = () => (
    <header className="App-header">
         <Button type="ghost" style={{margin:'5px', color: '#fff'}} onClick={()=> {window.location ="/"}}>
         Home
         </Button>
         <Button type="ghost" style={{margin:'5px', color: '#fff'}} onClick={()=> {window.location ="/market"}}>
         Market
         </Button>
         <Button type="ghost" style={{margin:'5px', color: '#fff'}} onClick={()=> {window.location ="/market-stats"}}>
         Market Stats
         </Button>
         <Button type="ghost" style={{margin:'5px', color: '#fff'}} onClick={()=> {window.location ="/invest"}}>
         Invest
         </Button>
         
     </header>           
);
  
export default Navbar;
 
