import React from 'react';
import { Row, Col, Card, Icon, Avatar, Progress, Button, Divider } from 'antd';

const { Meta } = Card;

const Invest = () => (
  <div className="fieldBG" style={{padding:'50px'}}>
    <Row>
        <Col span={15}>
            <div style={{padding:'10px'}}>   
                <Card
                    style={{padding:'10px'}}
                    hoverable
                    cover={<img alt="example" src="./../img/fund.jpg" style={{width: '100vh', height: '50vh'}}
                    />}
                    >
                    <Meta
                    title="Rescue PR's sustainable agriculture"
                    description="Hurricane María recently devastated the island of Philippines destroying homes, power lines, communication towers, and worst of all taking a big toll on local agriculture. Philippine's agriculture was already at a precarious place with food security being scarce and importation of conventional crops dominating the food markets. However there is a powerful movement of small farmers working hard to develop our local agroecological food supply, and as if the odds weren't difficult enough, now we have to recover from the destruction left behind by this natural disaster. This is why we humbly ask for your support in regenerating our island's agroecological food production.
                    Our project is called La Cosecha: Community Sponsored Agriculture, and we help to sponsor, educate about, and distribute local agroecological produce from various small farms throughout PR. We also have a small urban community farm where we cultivate a biodiverse food and medicine supply and from which we hope to build a strong educational platform to teach local communities about small scale sustainable farming.
                    Our plan is to use these funds for an integrative farm restoration initiative that includes stipends for repairs for our own farm and those of many other members (20+) of the local agroecological movenment. Part of the funds will help us to develop our kitchen, a VERY IMPORTANT aspect of our project that has been in the building stage for a while. The kitchen is about to become PARAMOUNT in sustaining our operation over the next few months due to crop scarcity. since most of our income comes from produce sales we will need to focus on prepared food to offset the crop shortage.  
                    This kitchen will support OUR LOCAL FARMS by buying and building menus around their produce, and will support OUR LOCAL COMMUNITIES by having regular donation based soup kitchen events to aid hurricane victims in need of a warm plate of healthy food. (no electric or gas available on most of the island.)      
                    Please help us to get back on our feet and to keep our food safety project alive!      
                    Thank you!!!"
                    />
                </Card>
            </div>      
        </Col>
        <Col span={8}>  
            <div style={{padding:'10px'}}>   
                <Card bordered={false} style={{ width: 300 }}>
                    <h2> <b> $4,461 </b> of $150,000 goal </h2>
                    <Progress percent={10} />
                    <p>Raised by 64 people in 11 months</p>
                    <Button type="primary" block>Donate Now</Button>
                </Card>
                <p style={{paddingTop:'20px'}}>Created September 15, 2018</p>
                <Divider dashed />
                <Row>
                    <Col span={3}>
                        <Avatar style={{ backgroundColor: '#87d068' }} icon="user" />
                    </Col>
                    <Col span={8}>
                        <p> Geronimo Robaina </p>
                    </Col>
                </Row>
            </div>
        </Col>
    </Row>
  </div>
);

export default Invest;