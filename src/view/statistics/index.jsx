import React from 'react';
import { Card, Row, Col } from 'antd';

const Statistics = () => (
  <div className="fieldBG2" style={{height: '100vh'}} >
  <br/><br/><br/>
  <Row type="flex" align="center" justify="center">
    <Col>
    <Card style={{borderRadius: '4px', margin: '8px'}}>
    <div>
      <table class="chartTable">
        <tr style={{background: '#dadada'}}>
          <th  className="chartTableBars"><div style={{height: "50px", width:"30px"}} className="chartRed"></div></th>
          <th  className="chartTableBars"><div style={{height: "56px", width:"30px"}} className="chartRed"></div></th>
          <th  className="chartTableBars"><div style={{height: "200px", width:"30px"}} className="chartRed"></div></th>
          <th  className="chartTableBars"><div style={{height: "150px", width:"30px"}} className="chartRed"></div></th>
        </tr>
        <tr>
          <th className="chartTableBottomTimeUnits">JAN</th>
          <th className="chartTableBottomTimeUnits">FEB</th>
          <th className="chartTableBottomTimeUnits">MAR</th>
          <th className="chartTableBottomTimeUnits">APR</th>
          </tr>
      </table>
  </div><br/>
  Recent Rating of Product #1
    </Card>
    </Col>
    <Col>
    <Card style={{borderRadius: '4px', margin: '8px'}}>
    <div>
      <table class="chartTable">
        <tr style={{background: '#dadada'}}>
          <th  className="chartTableBars"><div style={{height: "150px", width:"30px"}} className="chartRed"></div></th>
          <th  className="chartTableBars"><div style={{height: "90px", width:"30px"}} className="chartRed"></div></th>
          <th  className="chartTableBars"><div style={{height: "200px", width:"30px"}} className="chartRed"></div></th>
          <th  className="chartTableBars"><div style={{height: "180px", width:"30px"}} className="chartRed"></div></th>
        </tr>
        <tr>
          <th className="chartTableBottomTimeUnits">JAN</th>
          <th className="chartTableBottomTimeUnits">FEB</th>
          <th className="chartTableBottomTimeUnits">MAR</th>
          <th className="chartTableBottomTimeUnits">APR</th>
          </tr>
      </table>
  </div><br/>
  Recent Rating of Product #33
    </Card>
    </Col>
    <Col>
    <Card style={{borderRadius: '4px', margin: '8px'}}>
    <div>
      <table class="chartTable">
        <tr style={{background: '#dadada'}}>
          <th  className="chartTableBars"><div style={{height: "50px", width:"30px"}} className="chartRed"></div></th>
          <th  className="chartTableBars"><div style={{height: "200px", width:"30px"}} className="chartRed"></div></th>
          <th  className="chartTableBars"><div style={{height: "10px", width:"30px"}} className="chartRed"></div></th>
          <th  className="chartTableBars"><div style={{height: "120px", width:"30px"}} className="chartRed"></div></th>
        </tr>
        <tr>
          <th className="chartTableBottomTimeUnits">JAN</th>
          <th className="chartTableBottomTimeUnits">FEB</th>
          <th className="chartTableBottomTimeUnits">MAR</th>
          <th className="chartTableBottomTimeUnits">APR</th>
          </tr>
      </table>
  </div><br/>
  Recent Rating of Product #12
    </Card>
    </Col>
  </Row>
  </div>
);

export default Statistics;