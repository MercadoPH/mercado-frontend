import React from 'react';
import { Divider } from 'antd';
import '../../App.css';

import HeroView from '../../component/HeroView';
import Testimony from '../../component/Testimony';
import CoinInfo from '../../component/CoinInfo';


const Home = () => (
  <div className="App">
  <HeroView />
  <Testimony/><br/>
  <Divider/>
  <CoinInfo />
</div>

);

export default Home;