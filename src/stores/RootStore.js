import {notification} from 'antd'
import Web3Store from './Web3Store';
import AuthStore from './AuthStore';

class RootStore {  
    constructor() {
        this.web3Store = new Web3Store(this);
        this.authStore = new AuthStore(this);
    }

    openNotif = (type, message, description) => {
        if (description) {
          notification.open({
            type,
            message,
            description,
          });
        } else {
          notification.open({
            type,
            message,
          });
        }
      }

    successNotif = (message, description) => {
        this.openNotif('success', message, description);
      }
    
      errorNotif = (message, description) => {
        this.openNotif('error', message, description);
      }
}

export default RootStore;