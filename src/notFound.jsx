import React from 'react';

const PageNotFound = () => (
  <div style={{padding:'50px'}}>
    <h1>
      Uh oh... Well this is awkward.
    </h1>
    <span>we can't find the page that you're looking for.</span>
  </div>
);

export default PageNotFound;