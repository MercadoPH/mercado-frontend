import React from 'react';
import { Avatar, Row, Col } from 'antd';

const Testimony = () => (
  <div className="Testimony">
    <Row type="flex" gutter={16} align="center">
    <Col>
      <Avatar className="TBox" shape="square"/><br/><br/>
      <span className="TBoxText">Secure Transactions through Smart Contracts</span><br/><br/>
    </Col>
    <Col>
    <Avatar className="TBox" shape="square"/><br/><br/>
    <span className="TBoxText">No Hidden charges on Transactions</span><br/><br/>
    </Col>
      <Col>
      <Avatar className="TBox" shape="square"/><br/><br/>
      <span className="TBoxText">Fair Rewards through a Tokenized System</span><br/><br/>
      </Col>
    </Row>
  </div>
);

export default Testimony;