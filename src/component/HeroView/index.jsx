import React from 'react';
import { Avatar, Button } from 'antd';
import '../../App.css'; // style



const HeroView = () => (
  <div className="HeroView">
      <Avatar shape="square" className="Logo" src="/img/merkado.png"/>
      <div className="HeroText">
        Merkado PH
      </div>
      <span className="HeroText2">A decentralized platform for our Beloved Filipino Farmers</span>
      <br/><Button type="ghost" className="Nav1" onClick={()=>{window.location = 'market'}}>Get Started Today!</Button>
  </div>
);

export default HeroView;