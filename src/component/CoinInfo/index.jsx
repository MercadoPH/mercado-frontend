import React from 'react';
import {Row, Avatar, Col} from 'antd';

const CoinInfo = () => (
  <div>
  <Row 
  type="flex" 
  align="center">
          <Col>
            <Avatar style={{width: '200px', height: '200px' }} src="/img/umacoin.png"/><br/>
          </Col>
          <Col><br/>
            <div style={{fontSize:'20px'}}>What is UMA coin?</div>
            <br/>
            <div style={{ margin: '20px'}}>
            Is an Ethereum token used to fuel the Merkado PH which is a Decentralized Web application
             made with blockchain technology, that will serve as a platform for farmers, 
             sellers and other investors to interact and form as a scalable ecosystem.
            </div>
          </Col>
          </Row>
  </div>
);

export default CoinInfo;